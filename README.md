# FE-Workflow

The AMBER Drug Discovery Boost package







The AMBER Drug Discovery Boost package consists of three components, 1) FE-MDEngine, 2) FE-ToolKit,
and 3) FE-Workflow. FE-MDEngine consists of the current release version of AmberTools21 and a special-
ized version of AMBER20 (the pmemd package) that contains the newest technologies related to alchemical
free energy methods that are not yet available in the current release version of AMBER (AMBER20).
The AMBER License and “patch” code mechanism enables anyone with a current AMBER license to gain
advanced access to the AMBER DD Boost package for beta testing and validation prior to their official
integration into AMBER (which occurs on a 2-year cycle). FE-ToolKit consists of a collection of programs
that implement the latest approaches for analyzing free energy simulations, and FE-Workflow consists of a
collection of programs that are useful for the setup of alchemical free energy simulations (relative binding
free energy (RBFE), relative solvation free energy (RSFE), and absolute solvation free energy (ASFE))
using FE-MDEngine, and analysis of such simulations using FE-Workflow.
Below is a list of recent references related to the different features within AMBER DD Boost package,
as of February 15, 2022.

1. Tai-Sung Lee, Hsu-Chun Tsai, Abir Ganguly, Timothy J. Giese, and Darrin M. York. Robust, Efficient
and Automated Methods for Accurate Prediction of Protein-Ligand Binding Affinities in AMBER Drug
Discovery Boost, volume 1397 of ACS Symposium Series. November 2021. ISBN 9780841298064. doi:
10.1021/bk-2021-1397.ch007
2. Zoe Cournia, Christophe Chipot, Benoît Roux, Darrin M. York, and Woody Sherman. Free Energy
Methods in Drug Discovery—Introduction, volume 1397 of ACS Symposium Series. November 2021.
ISBN 9780841298064. doi: 10.1021/bk-2021-1397.ch001. URL https://pubs.acs.org/sharingguidelines
3. Timothy J. Giese and Darrin M. York. Variational Method for Networkwide Analysis of Relative
Ligand Binding Free Energies with Loop Closure and Experimental Constraints. J. Chem. Theory
Comput., 17(3):1326–1336, 2021
4. Tai-Sung Lee, Bryce K. Allen, Timothy J. Giese, Zhenyu Guo, Pengfei Li, Charles Lin, T. Dwight McGee
Jr., David A. Pearlman, Brian K. Radak, Yujun Tao, Hsu-Chun Tsai, Huafeng Xu, Woody Sherman,
and Darrin M. York. Alchemical Binding Free Energy Calculations in AMBER20: Advances and Best
Practices for Drug Discovery. J. Chem. Inf. Model., 60:5595–5623, 2020
5. Tai-Sung Lee, Zhixiong Lin, Bryce K. Allen, Charles Lin, Brian K. Radak, Yujun Tao, Hsu-Chun
Tsai, Woody Sherman, and Darrin M. York. Improved Alchemical Free Energy Calculations with
Optimized Smoothstep Softcore Potentials. J. Chem. Theory Comput., 16:5512–5525, September
2020. ISSN 1549-9626. doi: 10.1021/acs.jctc.0c00237
6. Hsu-Chun Tsai, Yujun Tao, Tai-Sung Lee, Kenneth M. Merz, and Darrin M. York. Validation of Free
Energy Methods in AMBER. J. Chem. Inf. Model., 60:5296–5300, November 2020. ISSN 1549-960X.
doi: 10.1021/acs.jcim.0c00285
